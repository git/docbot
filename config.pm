#!/usr/bin/perl


package docbot::config;
#
# config class for docbot
#

# config file schema:
#
# key = value
# key= value
# key =value
# key=value
# key=
# key =   value
# ...

# usage:
#
# $config = docbot::config->new(<configfile>);
# $config->set_autosave(<on/off>);
# $var = $config->get_key(<key name>);
# $config->set_key(<key name>, <new value>);
# $config->delete_key(<key name>);
# %var = $config->get_config_keys();
# $config->save_config();  # <configfile> from new() will be used
# $config->save_config(<new filename>);



use strict;
use POSIX; # some standards
use FileHandle; # have variables for the filehandles

# new()
#
# constructor
#
# parameter:
#  - class name
# return:
#  - pointer to config class
sub new {
  my $class = shift;
  # get config file
  my $config_file = shift;
  # test if config file exists
  if (!-f $config_file) {
    die "could not find config file: $config_file\n";
  }
  my $self = {};
  # bless mysqlf
  bless($self, $class);
  # define own variables
  # open config
  $self->{config} = $self->open_config($config_file);
  if (!defined($self->{config})) {
    die "could not open/parse config file: $config_file\n";
  }
  # save config file name for later use
  $self->{config_file} = $config_file;
  # set config to 'not changed'
  $self->{changed} = 0;
  # deactivate auto-save mode
  $self->{autosave} = 0;

  # return reference
  return $self;
}

# open_config()
#
# read in a config file
#
# parameter:
#  - self
#  - config filename
# return:
#  - reference to hash with config values
sub open_config {
  my $self = shift;
  if (!ref($self)) {
    die "do not call me from outside!\n";
  }
  # should be already validated for existence in new()
  my $config_file = shift;
  my $fh = new FileHandle;
  # define hash for config
  my %config = ();
  open($fh, $config_file) || die "could not open config file ($config_file): $!\n";
  # read every line
  while (my $line = <$fh>) {
    # remove any line ending char
    $line =~ s/^(.*?)[\s\r\n]*$/$1/g;
    if ($line =~ /^([a-zA-Z0-9\-_]+)[\s\t]*=[\s\t]*(.*)$/) {
      $config{$1} = $2;
      #print "read config line: $1 -> $2\n";
    }
  }
  close($fh);
  # return config
  return \%config;
}

# set_autosave()
#
# set autosave mode for config
#
# parameter:
#  - self
#  - autosave mode (on/off)
# return:
#  none
sub set_autosave {
  my $self = shift;
  my $autosave = shift;
  # validate the given mode
  if ($autosave eq "1" or lc($autosave) eq "yes" or lc($autosave) eq "on") {
    # set to 'on'
    $self->{autosave} = 1;
  } elsif ($autosave eq "0" or lc($autosave) eq "no" or lc($autosave) eq "off") {
    # set to 'off'
    $self->{autosave} = 0;
  } else {
    die "could not recognize autosave mode: $autosave (please use on/off)\n";
  }
}

# DESTROY()
#
# destructor
#
# parameter:
#  - self
# return:
#  none
sub DESTROY {
  my $self = shift;
  # check autosave mode
  if ($self->{autosave} == 1) {
    # check if config is saved
    if ($self->{changed} == 1) {
      # save the config
      $self->save_config();
    }
  }
}

# get_key()
#
# return a config value
#
# parameter:
#  - self
#  - config key name
# return:
#  - value of config parameter (or undef)
sub get_key {
  my $self = shift;
  my $key = shift;
  #  return value
  return $self->{config}->{$key};
}

# set_key()
#
# set a new config value
#
# parameter:
#  - self
#  - config key name
#  - new value
# return:
#  none
sub set_key {
  my $self = shift;
  my $key = shift;
  my $new_value = shift;
  # set new value
  $self->{config}->{$key} = $new_value;
  # mark config changed
  $self->{changed}  = 1;
}

# delete_key()
#
# delete a config key
#
# parameter:
#  - self
#  - config key name
# return:
#  none
sub delete_key {
  my $self = shift;
  my $key = shift;
  # delete key
  delete($self->{config}->{$key});
  # mark config changed
  $self->{changed}  = 1;
}

# get_config_keys()
#
# return hash with all defined config keys
#
# parameter:
#  - self
# return:
#  - hash with config keys
sub get_config_keys {
  my $self = shift;
  # return sorted hash
  return sort(keys(%{$self->{config}}));
}

# save_config()
#
# write the config to disk
#
# parameter:
#  - self
#  - (optional) config filename
# return:
#  none
# comment:
#  - if no filename is given, the original config filename will be used
sub save_config {
  my $self = shift;
  # get original config filename
  my $config_file = $self->{config_file};
  if (defined($_[0])) {
    # another config filename is given
    $config_file = shift;
  }
  my $fh = new FileHandle;
  # open config file for write
  open($fh, ">$config_file") || die "could not open config file for write ($config_file): $!\n";
  my ($key, $value);
  # get all keys
  foreach $key ($self->get_config_keys()) {
    # get the value for the key
    $value = $self->get_key($key);
    #print "write config line: $key -> $value\n";
    print $fh "$key = $value\n";
  }
  # flush the filehandle to get stuff written to disk
  $fh->flush;
  # close filehandle
  close($fh);
  #mark config unchanged
  $self->{changed} = 0;
}


# finish module
1;
